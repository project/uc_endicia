# README file for Endicia for Ubercart

What does Endicia for Ubercart do?
--------------------
This module integrates the label printing services provided by Grindflow Endicia
Cloud (GEC) to your Ubercart site, allowing to you generate and print postage
labels from quickly and conveniently after receiving orders.

It requires the Grindflow Endicia Cloud module available for download here:
* http://drupal.org/project/endicia


Requirements
------------
Endicia for Ubercart is an extension of the Grindflow Endicia Cloud module,
available here: http://drupal.org/project/endicia

Please keep in mind that while the Grindflow Endicia Cloud modules are free of
charge, a paid monthly subscription is required in order to print any labels.

It's easy! Sign up for an account today: http://grindflow.com/usps-grindflow-endicia-cloud-services


Installation
------------
For detailed instructions on how to install modules to your Drupal site, please
visit: http://drupal.org/documentation/install/modules-themes

1. Download & install Grindflow Endicia Cloud (see its README.txt for detailed
   instructions on how to do so).

2. Download & extract Endicia for Ubercart to your site modules folder.

3. Visit "Administer > Site Building > Modules" and enable the Endicia for
   Ubercart module.


Credits and Sponsors
--------------------
Development of this module was performed by Grindflow Management LLC,
http://www.grindflow.com.

If you require technical assistance, please contact support@grindflow.com.
